var questionList = [];
var wronglyAnsweredQuestions = [];
var testerDivider = null;
var nabidkaTable = null;
var currentQuestion = null;
var correctCheckBoxes = null;

$( document ).ready(function() {
    nabidkaTable = $("table#nabidkaTable");
    addQuestionsFromMemoryToNabidkaTable();
    testerDivider = $("div#tester");
    $("#cor").hide();
    $("#wro").hide();
    testerDivider.hide();
});


function selectSet(name) {
    hideNabidka();
    loadSet(name);
}

function hideNabidka() {
    $(nabidkaTable).toggle();
    $("p.podnadpis").toggle();
}

function loadSet(name) {
    if(name == null) {
        let parsedQuestions = JSON.parse(window.localStorage.getItem("wronglyAnsweredQuestionsStorage"));
        window.localStorage.removeItem("wronglyAnsweredQuestionsStorage");
        parsedQuestions.forEach(q => questionList.push(filterQuestion(q)));
        $("h1#nadpis").fadeOut(100).hide().text("Opakování").fadeIn(100);
        wronglyAnsweredQuestions = [];
        displayTester();
    } else {
        $.ajax({
            type: "GET", url: ("sety/" + name), dataType: "xml",
            success: function(xml) {
                questionList = [];
                parseReceivedXml(xml);
                $("h1#nadpis").fadeOut(100).hide().text($(xml).find("otazky").attr("nazev")).fadeIn(100);
                wronglyAnsweredQuestions = [];
                displayTester();
            },
            error: function() {
                $("#nadpis").text("Nepodařilo se stáhnout soubor " + name + ".").css("color", "#bd5a5a");
                $("<button type='button' class='btn btn-danger' onclick='location.reload()' style='margin-top: 25px; width: 150px;'>Zpět</button>").insertAfter($("#nadpis"));
            }
        });
    }
}

function displayTester() {
    $("#length").text(questionList.length);
    $("#done").text("0");
    displayQuestion();
    testerDivider.slideDown(600);
}

function parseReceivedXml(xml) {
    $(xml).find("otazky").children().each(function(index) {
        let spravne = [];
        let spatne = [];
        $(this).find("spravnaO").each(function(index) { spravne.push($(this).text()); });
        $(this).find("spatnaO").each(function(index) { spatne.push($(this).text()); });
        let finalQuestion = {zneni: $(this).find("zneni").text(),
                            spravne, spatne};
        questionList.push(finalQuestion);
        
    });
    //shuffle algo z stackoverflow
    questionList = questionList.map((value) => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);
}

function displayQuestion() {
    currentQuestion = questionList.pop();
    $("#answers").empty();
    $("#otazka").hide().text(currentQuestion.zneni).fadeIn(100);
    const rndOrderedAnswers = getAnswersInRandomizedOrder(filterQuestion(currentQuestion));
    let counter = 0;
    rndOrderedAnswers.forEach(
        q => {
            $("#answers").append('<div class="form-check"><input class="form-check-input" type="checkbox" value="" name="' + counter + '"><label class="form-check-label" for="flexCheckDefault">' + q + '</label></div>');
            counter += 1;
        });  
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function getAnswersInRandomizedOrder(question) {
    let arr = question.spatne;
    arr = arr.concat(question.spravne);
    correctCheckBoxes = [];
    arr = arr.map((value) => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);
    arr.forEach(function (item,index) {
        if(question.spravne.includes(item)) {
            correctCheckBoxes.push(index);
        }
    });
    return arr;
}

function sendButtonClicked() {
    var selected = [];
    $('.form-check input:checked').each(function() {
        selected.push(parseInt($(this).attr('name')));
    });
    if(arraysEqual(selected.sort(), correctCheckBoxes.sort())) {
        questionWasCorrectlyAnswered();
    } else {
        questionWasIncorrectlyAnswered();
    }
}

function questionWasCorrectlyAnswered() {
    incrementDone();
    $("#wro").hide();
    $("#cor").fadeIn(100).delay(1000).fadeOut(100);
    nextQuestion();
}

function questionWasIncorrectlyAnswered() {
    $("#cor").hide();
    $("#wro").fadeIn(100).delay(1000).fadeOut(100);
    var qs = filterQuestion(currentQuestion);
    questionList.unshift(qs);
    if(!wronglyAnsweredQuestions.includes(qs)) {
        wronglyAnsweredQuestions.push(qs);
    }
    nextQuestion();
}

function nextQuestion() {
    if(questionList.length == 0) {
        quizWasFinished();
        return;
    }
    $("#answers").slideUp(150);
    displayQuestion();
    $("#answers").slideDown(150);
}

function incrementDone() {
    const done = $("#done");
    $(done).text(parseInt($(done).text())+1);
}

function quizWasFinished() {
    $("#tester").hide();
    if(wronglyAnsweredQuestions.length == 0) {
        let congrats = "<h2 id='congrats'>Gratulujeme! V problematice se dobře orientujete. Kvíz byl zodpovězen bez chyb.</h2>";
        congrats += "<button type='button' id='toHmpg' class='btn btn-primary' onclick='backToHomepage()'>Pokračovat</button>";
        $(congrats).hide().insertAfter($("#nadpis")).fadeIn(300);
    } else {
        let congrats = "<h2 id='congrats'>V problematice ještě máte nějaké mezery. Přejete si špatně zodpovězené otázky uložit do ";
        congrats += "paměti prohlížeče, abyste se k nim pak mohli vrátit?</h2>";
        congrats += "<div id='toHmpg'><button type='button' class='btn btn-primary' onclick='saveToMemory()'>Ano</button>";
        congrats += "<button type='button' class='btn btn-danger' onclick='backToHomepage()'>Ne</button></div>"
        $(congrats).hide().insertAfter($("#nadpis")).fadeIn(300);
    }

}

function saveToMemory() {
    let tmp = JSON.parse(window.localStorage.getItem("wronglyAnsweredQuestionsStorage"));
    if(tmp) {
        tmp = tmp.concat(wronglyAnsweredQuestions);
        window.localStorage.setItem("wronglyAnsweredQuestionsStorage", JSON.stringify(tmp));

    } else {
        tmp = wronglyAnsweredQuestions;
        window.localStorage.setItem("wronglyAnsweredQuestionsStorage", JSON.stringify(tmp));

    }
    backToHomepage();
}
//TODO: Co když uživatel nic nevyplní?
function backToHomepage() {
    $("#congrats").slideUp(100);
    $("#toHmpg").slideUp(100);
    $("#nadpis").slideUp(150).hide().delay(50).text("Javascriptový TESTER").slideDown(150);
    $(".podnadpis").slideDown(150);
    addQuestionsFromMemoryToNabidkaTable();
    $(nabidkaTable).slideDown(150);
}

function addQuestionsFromMemoryToNabidkaTable() {
    $("tr#idk").remove();
    let tmp = JSON.parse(window.localStorage.getItem("wronglyAnsweredQuestionsStorage"));
    if(tmp != null) {
        const row = "<tr id='idk'><th scope='row'>Otázky co neumím (" + tmp.length + ")</th><td><button type='button' class='btn btn-primary' onclick='selectSet()'>Zvolit</button></td></tr>";
        $(row).insertAfter($("tr").last());
    }
}
function arraysEqual(array1, array2) {
    if (array1.length === array2.length) {
      return array1.every((element, index) => {
        if (element === array2[index]) {
          return true;
        }
        return false;
      });
    }
    return false;
}

function filterQuestion(q) {
    var tmp = q;
    const spr = new Set(tmp.spravne);
    tmp.spatne = tmp.spatne.filter((a) => {
        return !spr.has(a);
    });
    return tmp;
}